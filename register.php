<?php
if($_POST){
if(empty($_POST['fullname'])){
$fullnameerr="enter fullname!!";
}
else{
	$fullname=$_POST['fullname'];
}
if(empty($_POST['email'])){
$emailerr="enter email!!";
}
else{
	$email=$_POST['email'];
}
if(empty($_POST['password'])){
$passworderr="enter password!!";
}
else{
	$password=md5($_POST['password']);
}
if(empty($_POST['rpwd'])){
$rpwderr="reenter password!!";
}
if($_POST['password']!=$_POST['rpwd']){
	$wrongpassworderr="password not matched";
}

if(!empty($_POST['fullname']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['rpwd']))
if($_POST['password']===$_POST['rpwd']){
include "connection.php";

date_default_timezone_set("Asia/Kolkata");
$created=date("Y-m-d H:i:sa");

$select=mysqli_query($con,"select * from register where email='$email'");
$count=mysqli_num_rows($select);
if($count>0){
$emailexisterr="Email already exists";	
}
else{
$insert=mysqli_query($con,"insert into register (fullname,email,password,created) values ('$fullname','$email','$password','$created')");
header("location:index.php");
}
}	
}


?>



<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Online Shopping</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

</head>
<body class="hold-transition register-page">

<div class="register-box">
  <div class="register-logo">
    <a href="#"><b>Online</b>Shop</a>
  </div>
<?php if(isset($emailexisterr)) {?>
<div id="alert">
<div class="alert alert-danger register-box"><?php echo $emailexisterr; ?></div>
</div>
<?php } ?>
  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="fullname" id="fullname"  placeholder="Full name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
		<p class="text-danger"><?php if(isset($fullnameerr)){echo $fullnameerr;} ?></p>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" id="email"  placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		<p class="text-danger"><?php if(isset($emailerr)){echo $emailerr;} ?></p>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
		<p class="text-danger"><?php if(isset($passworderr)){echo $passworderr;} ?></p>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="rpwd" id="rpwd" placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
		<p class="text-danger"><?php if(isset($rpwderr)){echo $rpwderr;} ?></p>
		<p class="text-danger"><?php if(isset($wrongpassworderr)){echo $wrongpassworderr;} ?></p>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" name="register" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a>
    </div>

    <a href="index.php" class="text-center">I already have a membership</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 2.2.3 -->
<script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="assets/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
